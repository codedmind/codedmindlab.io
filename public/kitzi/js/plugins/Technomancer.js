//=============================================================================
// Technomancer.js
//=============================================================================

/*:
 * @plugindesc Various utilities and functions.
 * @author Kaiser
 *
 * @param Tactics Cursor Character
 * @desc Character set image file for Tactics cursor.
 * @default !Flame
 * 
 * @param Tactics BGM
 * @desc BGM for when Tactics starts, "none" to disable.
 * @default none
 * 
 * @param Tactics Cursor Index
 * @desc Character set image file index for Tactics cursor.
 * @default 5
 * 
 * @param Tactics Default Move Range
 * @desc Tile range character can move.
 * @default 5
 * 
 * @param Steal Default Odds
 * @desc Default chance of stealing.
 * @default 100
 * 
 * @param Tactics Enabled Switch
 * @desc ID of switch used for to store wether Tactics mode is enabled
 * @default 27
 * 
 * @help 
 * 
 * == Abilities ==
 * Create an ability that calls to a common event which calls the following scripts;
 * 
 * Technomancer.Abilities.doScan() shows a textbox with information of the targetted enemy
 * TODO description tag to have text in the text box
 * 
 * Technomancer.Abilities.doSteal() attempts to steal an item from the enemy
 * Steals are defined by tags in the enemy note tags
 * <steal:i4> says the enemy has the ITEM at ID 4
 * <steal:w2> is for the WEAPON at ID 2
 * <steal:a9> is for the ARMORS at ID 9
 * <steal:g1000> says the player will steal 1000 gold <TODO add some variance to the random amount range around the defined number>
 * == Cheats ==
 * 
 * Technomancer.Cheats.giveAll()
 * 
 * == Tactics Battle system ==
 * 
 * == Utilities ==
 */

var Technomancer = Technomancer || {};  
Technomancer.Abilities = Technomancer.Abilities || {};  
Technomancer.Cheats = Technomancer.Cheats || {};  
Technomancer.Tactics = Technomancer.Tactics || {};  
Technomancer.Utils = Technomancer.Utils || {};
Technomancer.Instances = Technomancer.Instances || {};
Technomancer.Tactics.turns = 0; //how many turns has passed
Technomancer.Tactics.turn = 0; // 0 = player, 1 = AI
Technomancer.Tactics.teams = {}; //index of all tactics-enabled events in map sorted by their group ID meta, "<tactics:(id)>"
Technomancer.Duel = Technomancer.Duel || {};
Technomancer.TeamUI = Technomancer.TeamUI || {};

(function() {
var _Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;

Technomancer.Duel.begin = function() {
    if(Technomancer.Instances.dui){
        Technomancer.Instances.dui.close();
        Technomancer.Instances.dui.destroy();
        console.log("Redrawing window.")
    }
    Technomancer.Instances.dui = new Window_Base(0, 0, 820, 100);
    //Technomancer.Instances.d2 = new Window_Base(650, 0, 200, 70);
    SceneManager._scene.addWindow(Technomancer.Instances.dui);
    //SceneManager._scene.addWindow(Technomancer.Instances.d2);
    //Technomancer.Instances.d1.drawTextEx("Player: 7100", 0, 0);
    
    var mc = new Game_Actor(1);
    var en = new Game_Actor(2);
    Technomancer.Instances.dui.drawActorHp(mc, 20, 20);
    Technomancer.Instances.dui.drawActorName(mc, 0, 0);
    Technomancer.Instances.dui.drawActorHp(en, 600, 20);
    Technomancer.Instances.dui.drawActorName(en, 570, 0);
}

Technomancer.Utils.range = function(low, high){ var arr = []; while(low <= high){ arr.push(low++); } return arr; }

Technomancer.Tactics.getValidMoves = function(e){
    var r = Technomancer.Utils.range;
    var mv = e.tacticsData.moveRange;
    var out = [];
    var x = e.x;
    var y = e.y;
    // X axis
    for(let coord of r(x-mv, x+mv)) out.add(coord);
    // Y axis
    for(let coord of r(y-mv, y+mv)) out.add(coord);
}
Technomancer.Tactics.select = function(x, y){
    var eid = $gameMap.eventIdXy(x, y);
    var ev = $gameMap.event(eid);
    if(ev != undefined){
        console.log(ev.event().meta)
        //$gamePlayer._tacticsEventSelect = ev;
        if(ev.event().meta.tactics == 0){
            const choices = ["Close", "Move", "Attack"];
            $gameMap._interpreter.setupChoices([choices, 1]);
            $gameMessage.setChoiceCallback(n => {

                if(n == 1){
                    $gamePlayer._tacticsEventSelect = ev;
                    Technomancer.Tactics.getValidMoves(ev);
                }
            });
        }
    }else{
        if($gamePlayer._tacticsEventSelect != null){
            const choices = ["Move", "Deselect", "Cancel"];
            $gameMap._interpreter.setupChoices([choices, 1]);
            $gameMessage.setChoiceCallback(n => {
                if(n == 0){
                    e = $gamePlayer._tacticsEventSelect
                    if(e.moved){
                        return;
                    }
                    var steps = e.tacticsData.moveRange;
                                           
                    e.setPosition($gamePlayer.x, $gamePlayer.y);
                    console.log("Done move.")
                    $gamePlayer._tacticsEventSelect = null;  
                    e.tacticsMoved = true;
                }
                if(n == 1){
                    $gamePlayer._tacticsEventSelect = null;
                }

            }); 
        }else{
            const choices = ["Close", "End Turn"];
            $gameMap._interpreter.setupChoices([choices, 1]);
            $gameMessage.setChoiceCallback(n => { console.log(n); });
        }

    }
    
}

Input.keyMapper[69] = 'tactics'; // e
Technomancer.Tactics.Game_Player_moveByInput = Game_Player.prototype.moveByInput;
Game_Player.prototype.moveByInput = function() {
    var en = Number(PluginManager.parameters('Technomancer')["Tactics Enabled Switch"]);
	if (this.canMove() && Input.isTriggered('tactics') && 
        $gameSwitches.value(en) == true && Technomancer.Tactics.turn == 0) {
        Technomancer.Tactics.select($gamePlayer.x, $gamePlayer.y)
	};
	Technomancer.Tactics.Game_Player_moveByInput.call(this);
};
Technomancer.Tactics.getCharacterByEvtId = function(group, evtid){
    for(var e of Technomancer.Tactics.teams[group]){
        if(e.id == evtid) return e;
    }
}
Technomancer.Tactics.getValidRange = function(range, x, y){
    var r = [];
    
}
Technomancer.Tactics.getLeader = function(grp_id) {
    var o = [];
    for(let e of Technomancer.Tactics.teams[grp_id]){
        if(e.leader) o.append(e);
    }
    
    return o;
}
Technomancer.Tactics.start = function(){
    $gamePlayer._preTactCharacterIndex = $gamePlayer._characterIndex;
    $gamePlayer._preTactCharacterName = $gamePlayer._characterName
    $gamePlayer._tacticsEventSelect = null;
    Technomancer.Tactics.teams = {};
    var steps = Number(PluginManager.parameters('Technomancer')["Tactics Default Move Range"]) || 5;
    for(var e of $gameMap.events()){ 
        if(e.event().meta.tactics != undefined){
            e.tacticsMoved = false;
            var group = e.event().meta.tactics;
            if(Technomancer.Tactics.teams[group] == undefined) Technomancer.Tactics.teams[group] = [];
            
            var tobj = {
                id: e.eventId(), 
                character: e, 
                evt: e.event(), 
                leader: e.event().meta.tacticsLeader,
                resourceType: null,
                moveRange: steps
            }
            if(e.event().meta.moveRange) tobj.moveRange = e.event().meta.moveRange;
            if(e.event().meta.tacticsParty){
                var actid = $gameParty._actors[Number(e.event().meta.tacticsParty)]
                if(actid != undefined){
                    tobj.character = $gameMap.events()[tobj.id];
                    $gameMap.events()[tobj.id-1]._characterName = $dataActors[actid].characterName;
                    $gameMap.events()[tobj.id-1]._characterIndex = e.event().meta.tactIndex;
                    if($gameMap.events()[tobj.id-1]._characterIndex == undefined) $gameMap.events()[tobj.id-1]._characterIndex = 0;
                    tobj.resourceType = "actor";
                    tobj.resource = $dataActors[Number(e.event().meta.tacticsParty)];   
                }

            }
            
            if(e.event().meta.tacticsEnemy){
                tobj.character = $gameMap.events()[tobj.id-1];
                $gameMap.events()[tobj.id-1]._characterName = $dataEnemies[Number(e.event().meta.tacticsEnemy)].meta.tacticsSprite;
                $gameMap.events()[tobj.id-1]._characterIndex = $dataEnemies[Number(e.event().meta.tacticsEnemy)].meta.tacticsSpriteIndex;
                if($gameMap.events()[tobj.id-1]._characterIndex == undefined) $gameMap.events()[tobj.id-1]._characterIndex = 0;
                tobj.resourceType = "enemy";
                tobj.resource = $dataEnemies[Number(e.event().meta.tacticsEnemy)];
            }
            
            e.tacticsData = tobj;
            Technomancer.Tactics.teams[group].push(tobj) 
        }       
    }
    
    var tact_cursor = PluginManager.parameters('Technomancer')["Tactics Cursor Character"];
    var tact_index = Number(PluginManager.parameters('Technomancer')["Tactics Cursor Index"]);
    var en = Number(PluginManager.parameters('Technomancer')["Tactics Enabled Switch"]);
    $gamePlayer._characterName = tact_cursor;
    $gamePlayer._characterIndex = tact_index;
    $gamePlayer._through = true;
    $gameSwitches.setValue(en, true);
    
    var tact_bgm_file = PluginManager.parameters('Technomancer')["Tactics BGM"];
    if(tact_bgm_file != "none"){
        $gamePlayer._tactAudioChanged = true;
        $gameSystem.saveBgm();
        var tact_bgm = { name: tact_bgm_file, volume: 90, pitch: 100, pan: 0 }
        AudioManager.playBgm( tact_bgm );
    }
    
    // TODO iterate over all events in map and sort them in to teams based on their .event().meta.tactics
}

Technomancer.Tactics.end = function(){
    console.log("Ending Tactics")
    $gamePlayer._characterName = $gamePlayer._preTactCharacterName;
    $gamePlayer._characterIndex = $gamePlayer._preTactCharacterIndex;
    var en = Number(PluginManager.parameters('Technomancer')["Tactics Enabled Switch"]);
    $gamePlayer._through = false;
    $gameSwitches.setValue(en, false);
    
    if($gamePlayer._tactAudioChanged) $gameSystem.replayBgm();
}
//this.character(eval(args.shift()));
Game_Interpreter.prototype.pluginCommand = function(command, args) {
    _Game_Interpreter_pluginCommand.call(this, command, args);

        // $gameMap.events()[<id>].event().meta
    if (command === 'Tactics') {
        let onoroff = args.shift()
        if(onoroff == "on"){
            Technomancer.Tactics.start();
        } else  if(onoroff == "off"){
            Technomancer.Tactics.end();
        }
    }

};  
    
Technomancer.Cheats.giveAll = function(){
    $dataItems.forEach(function(item) {$gameParty.gainItem(item,99,false);});
    $dataArmors.forEach(function(armor){ $gameParty.gainItem(armor,1,false);});
    $dataWeapons.forEach(function(weapon){ $gameParty.gainItem(weapon,1,false);});
}
function randRanger(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
}


function getInvName(code){
    if(code == undefined) return []
    var st = code[0];
    var si = Number(code.slice(1))
    if(st == "i"){
        var item = $dataItems[si]
        return [item.iconIndex, item.name]
    }
    
    if(st == "a"){
        var item = $dataArmors[si]
        return [item.iconIndex, item.name]
    }
    
    if(st == "w"){
        var item = $dataWeapons[si]
        return [item.iconIndex, item.name]
    }

}

Technomancer.Abilities.doScan = function(){
    var en = $gameTroop._enemies[BattleManager._action._targetIndex];
    var id = en._enemyId;
    var stealstr = ""
    let item = getInvName(en.enemy().meta["steal"]);
    if(en.enemy().meta.steal != undefined && !en.stolen) stealstr = `\nIs carrying \\i[${item[0]}]${item[1]}.`
    
    var body = `  \\c[2]${en.originalName()}\\c[0]
    \\i[84] \\c[2]${en.hp}\\c[0]/\\c[2]${en.mhp}\\c[0]
    \\i[87] \\c[2]${en.mp}\\c[0]/\\c[2]${en.mmp}\\c[0] ${stealstr}`
    $gameMessage.add(body);
}

Technomancer.Abilities.doSteal = function(){
    var odds = Number(PluginManager.parameters('Technomancer')["Steal Default Odds"]);
    console.log(`def ${odds}`);
    
    var en = $gameTroop._enemies[BattleManager._action._targetIndex]
    var ss = en.enemy().meta["steal"]
    
    var new_odds = en.enemy().meta["steal_odds"]
    if(new_odds != undefined){
        odds = Number(new_odds);
        console.log(`Updated odds to ${odds}`)
    }

    if(en.stolen || ss == undefined){ $gameMessage.add("Not carrying anything!"); return; }
    let cando = randRanger(0, 100)
    if (cando <= odds){
        var st = ss[0];
        var si = Number(ss.slice(1))
        if(st == "i"){
            var item = $dataItems[si]
            $gameMessage.add("Stole item: \\i["+item.iconIndex+"]"+item.name)
            $gameParty.gainItem(item, 1, false);
            en.stolen = true;
        } //item
        if(st == "a"){
            var item = $dataArmors[si]
            $gameMessage.add("Stole equipment: \\i["+item.iconIndex+"]"+item.name)
            $gameParty.gainItem($dataArmors[si], 1, false);
            en.stolen = true;
        } //armor
        if(st == "w"){
            var item = $dataWeapons[si]
            $gameMessage.add("Stole weapon: \\i["+item.iconIndex+"]"+item.name)
            $gameParty.gainItem($dataWeapons[si], 1, false);
            en.stolen = true;
        } //weapon
        if(st == "g"){
            $gameMessage.add("Stole money: "+si);
            $gameParty.gainGold(si);
            en.stolen = true;
        } //gold
        
    }else{
        $gameMessage.add("Steal missed!")
    }

}
})();
